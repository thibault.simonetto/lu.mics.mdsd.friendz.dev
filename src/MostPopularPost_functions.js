module.exports = {
  /**
   * Root.t:
   * @param findBest The "findBest" of this Root
   */
  Root_t: function(findBest) {
    return findBest // TODO
  },

  /**
   * Root.findBest:
   * @param cont_Post_popularityRateAndContent {Array} The sequence of "popularityRateAndContent" values of Post objects contained in this Root
   */
  Root_findBest: function(cont_Post_popularityRateAndContent) {
    if (cont_Post_popularityRateAndContent.length > 0) {
      formatted = cont_Post_popularityRateAndContent.map(x => {x = x.split("kdvnvnonvonsdnvodsnvc;jksdbvabls"); x[1] = parseInt(x[1]); return x} )
      formatted.sort((a, b) => (a[1] < b[1]) ? 1 : -1)
      if (formatted.length )
      return "Most popular post is \"" + formatted[0][0] + "\" with " + formatted[0][1] + " like(s) and share(s)."
    } else {
      return "There is no post."
    }


  },

  /**
   * Post.popularityRateAndContent:
   * @param content The "content" of this Post
   * @param popularityRate The "popularityRate" of this Post
   */
  Post_popularityRateAndContent: function(content, popularityRate) {
    return content + "kdvnvnonvonsdnvodsnvc;jksdbvabls" + popularityRate
  },

  /**
   * Post.popularityRate:
   * @param _likes_Member_name {Set} The set of "name" values of Member objects that refer to this Post by attribute "likes"
   * @param _shares_Member_name {Set} The set of "name" values of Member objects that refer to this Post by attribute "shares"
   */
  Post_popularityRate: function(_likes_Member_name, _shares_Member_name) {
    rate = Array.from(_likes_Member_name).length + Array.from(_shares_Member_name).length
    return "" + rate;
  },

};
