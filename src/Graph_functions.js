module.exports = {
  /**
   * Root.t:
   * @param members The "members" of this Root
   * @param posts The "posts" of this Root
   * @param photos The "photos" of this Root
   * @param likes The "likes" of this Root
   * @param shares The "shares" of this Root
   * @param relatesTo The "relatesTo" of this Root
   * @param publishes The "publishes" of this Root
   * @param friends The "friends" of this Root
   */
  Root_t: function(members, posts, photos, likes, shares, relatesTo, publishes, friends) {
    return "digraph {\n"+members+"\n"+posts+"\n"+photos+"\n"+likes+"\n"+shares+"\n"+relatesTo+publishes+"\n"+friends+"\n"+"\n}";
  },

  /**
   * Root.members:
   * @param cont_Member_styleGraphNode {Array} The sequence of "styleGraphNode" values of Member objects contained in this Root
   */
  Root_members: function(cont_Member_styleGraphNode) {
    return cont_Member_styleGraphNode.join("\n");
  },

  /**
   * Member.styleGraphNode:
   * @param graphNode The "graphNode" of this Member
   */
  Member_styleGraphNode: function(graphNode) {
    return graphNode + "[shape=circle]";
  },

  /**
   * Member.graphNode:
   * @param name The "name" of this Member
   */
  Member_graphNode: function(name) {
    return "\"" + name + "\"";
  },

  /**
   * Root.posts:
   * @param cont_Post_styleGraphNode {Array} The sequence of "styleGraphNode" values of Post objects contained in this Root
   */
  Root_posts: function(cont_Post_styleGraphNode) {
    return cont_Post_styleGraphNode.join("\n");
  },

  /**
   * Post.styleGraphNode:
   * @param graphNode The "graphNode" of this Post
   */
  Post_styleGraphNode: function(graphNode) {
    return graphNode + "[shape=box]";
  },

  /**
   * Post.graphNode:
   * @param content The "content" of this Post
   */
  Post_graphNode: function(content) {
    return "\"" + content + "\"";
  },

  /**
   * Root.photos:
   * @param cont_Photo_styleGraphNode {Array} The sequence of "styleGraphNode" values of Photo objects contained in this Root
   */
  Root_photos: function(cont_Photo_styleGraphNode) {
    return cont_Photo_styleGraphNode.join("\n");
  },

  /**
   * Photo.styleGraphNode:
   * @param graphNode The "graphNode" of this Photo
   */
  Photo_styleGraphNode: function(graphNode) {
    return graphNode + "[shape=hexagon,color=blue]";
  },

  /**
   * Photo.graphNode:
   * @param label The "label" of this Photo
   */
  Photo_graphNode: function(label) {
    return "\"" + label + "\"";
  },

  /**
   * Root.likes:
   * @param cont_Member_styleLikesEdges {Array} The sequence of "styleLikesEdges" values of Member objects contained in this Root
   */
  Root_likes: function(cont_Member_styleLikesEdges) {
    return cont_Member_styleLikesEdges.join("\n")
  },

  /**
   * Member.styleLikesEdges:
   * @param likes_Post_graphNode {Array} The sequence of "graphNode" values of Post objects referred to by attribute "likes" in this Member
   * @param likes_Photo_graphNode {Array} The sequence of "graphNode" values of Photo objects referred to by attribute "likes" in this Member
   * @param graphNode The "graphNode" of this Member
   */
  Member_styleLikesEdges: function(likes_Post_graphNode, likes_Photo_graphNode, graphNode) {
    likes = likes_Post_graphNode.concat(likes_Photo_graphNode)
    return likes.map(x => graphNode + " -> " + x + " [color=green]").join("\n")  // TODO
  },

  /**
   * Root.shares:
   * @param cont_Member_styleSharesEdges {Array} The sequence of "styleSharesEdges" values of Member objects contained in this Root
   */
  Root_shares: function(cont_Member_styleSharesEdges) {
    return cont_Member_styleSharesEdges.join("\n")
  },

  /**
   * Member.styleSharesEdges:
   * @param shares_Post_graphNode {Array} The sequence of "graphNode" values of Post objects referred to by attribute "shares" in this Member
   * @param shares_Photo_graphNode {Array} The sequence of "graphNode" values of Photo objects referred to by attribute "shares" in this Member
   * @param graphNode The "graphNode" of this Member
   */
  Member_styleSharesEdges: function(shares_Post_graphNode, shares_Photo_graphNode, graphNode) {
    shares = shares_Post_graphNode.concat(shares_Photo_graphNode)
    return shares.map(x => graphNode + " -> " + x + " [style=dashed]").join("\n")  // TODO
  },

  /**
   * Root.relatesTo:
   * @param cont_Photo_styleRelatesToEdges {Array} The sequence of "styleRelatesToEdges" values of Photo objects contained in this Root
   */
  Root_relatesTo: function(cont_Photo_styleRelatesToEdges) {
    return cont_Photo_styleRelatesToEdges.join("\n")
  },

  /**
   * Photo.styleRelatesToEdges:
   * @param relatesTo_Post_graphNode {Array} The sequence of "graphNode" values of Post objects referred to by attribute "relatesTo" in this Photo
   * @param graphNode The "graphNode" of this Photo
   */
  Photo_styleRelatesToEdges: function(relatesTo_Post_graphNode, graphNode) {
    return relatesTo_Post_graphNode.map(x => graphNode + " -> " + x + " [style=plain,color=blue,arrowtail=dot,dir=back]").join("\n") // TODO
  },

  /**
   * Root.publishes:
   * @param cont_Member_stylePublishesEdges {Array} The sequence of "stylePublishesEdges" values of Member objects contained in this Root
   */
  Root_publishes: function(cont_Member_stylePublishesEdges) {
    return cont_Member_stylePublishesEdges.join("\n")
  },

  /**
   * Member.stylePublishesEdges:
   * @param publishes_Post_graphNode {Array} The sequence of "graphNode" values of Post objects referred to by attribute "publishes" in this Member
   * @param publishes_Photo_graphNode {Array} The sequence of "graphNode" values of Photo objects referred to by attribute "publishes" in this Member
   * @param graphNode The "graphNode" of this Member
   */
  Member_stylePublishesEdges: function(publishes_Post_graphNode, publishes_Photo_graphNode, graphNode) {
    publishes = publishes_Post_graphNode.concat(publishes_Photo_graphNode)
    return publishes.map(x => graphNode + " -> " + x + " []").join("\n")  // TODO
  },

  /**
   * Root.friends:
   * @param cont_Friend_styleFriendEdges {Array} The sequence of "styleFriendEdges" values of Friend objects contained in this Root
   */
  Root_friends: function(cont_Friend_styleFriendEdges) {
    return cont_Friend_styleFriendEdges.join("\n")
  },

  /**
   * Friend.styleFriendEdges:
   * @param m1_Member_graphNode {Array} The sequence of "graphNode" values of Member objects referred to by attribute "m1" in this Friend
   * @param m2_Member_graphNode {Array} The sequence of "graphNode" values of Member objects referred to by attribute "m2" in this Friend
   */
  Friend_styleFriendEdges: function(m1_Member_graphNode, m2_Member_graphNode) {
    return  m1_Member_graphNode + " -> " + m2_Member_graphNode + " [color=green,dir=none]"  // TODO
  },

};
