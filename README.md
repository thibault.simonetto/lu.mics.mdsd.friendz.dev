This repository is organise as followed:
- the root contains the transformation configurations.
- `src` folder contains the sources (models, metamodels, fudomo decompositions, function implementations).
- `target` folder contains the result of the transformations.

# Sources

The sources contains the following files:
- `FriendzNet[X].yaml` file contains the model of the friendz network number X.
- `Metamodel.yaml` file contains the metamodel inferred from the 3 models.
- `MostPopularPost.fudomo` contains the function decomposition for the most popular post transformation.
- `MostPopularPost_functions.js` contains the JS implementation of the most popular post transformation functions.
- `Graph.fudomo` contains the function decomposition for the graph transformation.
- `Graph_functions.js` contains the JS implementation of the graph transformation functions.

# Transformation configuration

- `most-popular-post.config` contains the configuration of the most popular post transformation.
- `graph.config` contains the configuration of the graph transformation.

By default the execution of these configurations transform the 3rd model. Feel free to edit them to transform the other models.

# Target

The target contains the following files:
- `MostPopularPost[X].txt` file contains the result of the most popular post transformation of model X.
- `FriendzGraph[X].dot` file contains the result of the graph transformation of model X.
